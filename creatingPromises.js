// # Creating Promises
// Building your own promise based functions. First, solve it using promises. Then, refactor all solutions using async await.
function fetchRandomNumbers(){
    return new Promise((resolve,reject)=>{
        console.log("creating a random number");
     setTimeout (function generateRandomNumber(){
        let number =  Math.floor(Math.random() * Math.floor(10000));
        console.log("number generated");
        if(number === undefined)
        {
            reject("number undefined");
        }
        else{
        resolve(number);
        }
        return number;
       },1000)
    })
   
}


function fetchRandomString(){
    return new Promise((resolve,reject)=>{
        console.log("creating a random string");
     setTimeout (function generateRandomstring(){
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 6; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        let newString = result;
        if(newString  === undefined)
        {
            reject("string undefined");
        }
        else{
            console.log("string genrated");
        resolve(newString);
        }
       },1000)
    })
    
}
// **Task 1**: Right now, the function fetchRandomNumbers can be used by passing a callback,
// Your task is to promisfy this function so that the following can be done:

fetchRandomNumbers()
.then((number)=>{
   
    console.log(number);
})
.catch((err)=>{
    console.error("error occured");
    console.log(err);
})

// Similarlly, do the same for the function fetchRandomString
//fetching random string

fetchRandomString()
.then((word)=>{
    console.log(word);
})
.catch((err)=>{
    console.error("error occured");
    console.log(err);
})

async function randomNumber(){
    try{
       const value = await fetchRandomNumbers();
       console.log(value);
    }
    catch(err){
     console.error(err);
    }
}
async function randomString(){
    try{
       const value = await fetchRandomString(); 
       console.log(value);
    }
    catch(err){
     console.error(err);
    }
}
randomNumber();
randomString();

// **Task 2**: Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable
// -> add it to the same sum variable and print the sum variable.

fetchRandomNumbers()
.then((number)=>{
    console.log(number);
   let sum = number;
    return sum;
})
.catch((err)=>{
    console.error("error occured");
    console.log(err);
})
.then((sum)=>{
    fetchRandomNumbers()
    .then((num)=>{
        console.log(num);
        sum += num;

        console.log(sum ); 
    })
    .catch((err)=>{
        console.log("error occured while fetchng the number second time"+err);
    })
})
.catch(()=>{
console.log("error occured  ");
})

async function SumOfrandomNumber(){
    try{
        let sum =0;
       const value1 = await fetchRandomNumbers();
       sum += await value1;
      console.log("sum: "+sum );
      const value2 = await fetchRandomNumbers();
      sum += await value2;
      console.log("sum: "+sum);
    }
    catch(err){
     console.error(err);
    }
    
}
SumOfrandomNumber();

// **Task 3**: Fetch a random number and a random string simultaneously, concatenate their
// and print the concatenated string

async function concatedString(){
            
         try{
                let sum ;
               const value1 =  fetchRandomNumbers();
              const value2 =  fetchRandomString();
              sum = await value2+ await value1;
              console.log(sum);
            }
            catch(err){

            }
          
}
concatedString();

function genrateAndConcat(){
    let numbers ;
    let strings;
  fetchRandomNumbers()
    .then((number)=>{
        numbers = number;  
    })
    .catch((err)=>{
        console.log(err);
    })
    fetchRandomString()
.then((word)=>{
    strings = word;
})
.catch((err)=>{
    console.error("error occured");
    console.log(err);
})
    .then(()=>{
        let concated = strings+numbers.toString();
        console.log(concated)
    })

}
genrateAndConcat();


// **Task 4**: Fetch 10 random numbers simultaneously -> and print their sum.


function SumOfRandom(){
    let sum =0
    fetchRandomNumbers()
    .then((number)=>{
        //1
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
.then((number)=>{
    //2
    console.log(number)
 sum += number; 
})
.catch((err)=>{
    console.error("error occured");
    console.log(err);
}) 
fetchRandomNumbers()
    .then((number)=>{
        //3
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //4
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //5
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //6
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //7
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //8
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //9
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
    fetchRandomNumbers()
    .then((number)=>{
        //10
        console.log(number)
        sum += number;
       
    })
    .catch((err)=>{
        console.error("error occured");
        console.log(err);
    })
.then(()=>{
    console.log(sum);
})
}
SumOfRandom();
  async function sumOfTenRandomNumbers(){
      let value1 = fetchRandomNumbers();
      let value2 = fetchRandomNumbers();
      let value3 = fetchRandomNumbers();
      let value4 = fetchRandomNumbers();
      let value5 = fetchRandomNumbers();
      let value6 = fetchRandomNumbers();
      let value7 = fetchRandomNumbers();
      let value8 = fetchRandomNumbers();
      let value9 = fetchRandomNumbers();
      let value10 = fetchRandomNumbers();
      
      let sum = await value1+ await value2 + await value3+ await value4 + await value5+ await value6 +await value7+ await value8 + await value9+ await value10 
      console.log(sum);

  }
  sumOfTenRandomNumbers();